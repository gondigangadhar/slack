import React from "react";
import styled from "styled-components";
import TruncatedComponent from "../Common/TruncatedComponent";

function SidebarOption({ Icon, title }) {
  return (
    <SidebarOptionContainer>
      <Icon fontSize="small" />
      <TruncatedComponent styles={{ maxWidth: 180, paddingLeft: 6 }}>
        {title}
      </TruncatedComponent>
    </SidebarOptionContainer>
  );
}

export default SidebarOption;

const SidebarOptionContainer = styled.div`
  display: flex;
  align-items: center;
  font-size: 14px;
  color: #967b96;
  font-weight: 400;
  padding-left: 2px;
  margin: 8px;
  cursor: pointer;
  :hover {
    opacity: 0.9;
    background-color: #340e36;
  }
`;
