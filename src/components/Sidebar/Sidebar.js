import React from "react";
import styled from "styled-components";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import ForumRoundedIcon from "@mui/icons-material/ForumRounded";
import ChatRoundedIcon from "@mui/icons-material/ChatRounded";
import AlternateEmailRoundedIcon from "@mui/icons-material/AlternateEmailRounded";
import SendRoundedIcon from "@mui/icons-material/SendRounded";
import BookmarkBorderRoundedIcon from "@mui/icons-material/BookmarkBorderRounded";
import ConnectingAirportsRoundedIcon from "@mui/icons-material/ConnectingAirportsRounded";
import MoreVertRoundedIcon from "@mui/icons-material/MoreVertRounded";
import SidebarOption from "./SidebarOption";
import TruncatedComponent from "../Common/TruncatedComponent";

function Sidebar() {
  return (
    <SidebarContainer>
      <SidebarHeader>
        <SiderBarInfo>
          <TruncatedComponent
            styles={{ maxWidth: 150, fontSize: 15, fontWeight: "bold" }}
          >
            Slack
          </TruncatedComponent>
          <KeyboardArrowDownIcon size="small" />
        </SiderBarInfo>
        <BorderColorIcon />
      </SidebarHeader>
      <SidebarQuickLinks>
        <SidebarOption Icon={ForumRoundedIcon} title="Threads" />
        <SidebarOption Icon={ChatRoundedIcon} title="Direct messages" />
        <SidebarOption
          Icon={AlternateEmailRoundedIcon}
          title="Mentions & Reactions"
        />
        <SidebarOption Icon={SendRoundedIcon} title="Drafts & Sent" />
        <SidebarOption Icon={BookmarkBorderRoundedIcon} title="Saved Items" />
        <SidebarOption
          Icon={ConnectingAirportsRoundedIcon}
          title="Slack Connect"
        />
        <SidebarOption Icon={MoreVertRoundedIcon} title="More" />
      </SidebarQuickLinks>
    </SidebarContainer>
  );
}

export default Sidebar;

const SidebarContainer = styled.div`
  color: #fff;
  background-color: #3f0f40;
  flex: 0.3;
  height: 100vh;
  max-width: 260px;
`;

const SidebarHeader = styled.div`
  display: flex;
  padding: 12px;
  border-bottom: 1px solid #49274b;
`;

const SiderBarInfo = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`;

const SidebarQuickLinks = styled.div``;
