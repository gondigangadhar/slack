import React from "react";

function TruncatedComponent(props) {
  return (
    <div
      style={{
        ...props.styles,
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
      }}
    >
      {props.children}
    </div>
  );
}

export default TruncatedComponent;
