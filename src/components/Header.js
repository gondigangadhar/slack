import React from "react";
import styled from "styled-components";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import { AccessTime, SearchOutlined } from "@mui/icons-material";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";

function Header() {
  return (
    <>
      <HeaderContainer>
        <TimeIcon>
          <AccessTime />
        </TimeIcon>
        <Search>
          <SearchOutlined />
          <input type="text" />
        </Search>
        <UserInfo>
          <HelpOutlineIcon />
          <UserAvatar />
        </UserInfo>
      </HeaderContainer>
    </>
  );
}

export default Header;

const HeaderContainer = styled.div`
  display: flex;
  position: sticky;
  top: 0;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  padding: 10px 0;
  background-color: #350d36;
  color: #fff;
`;

const UserInfo = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: flex-end;
  > svg {
    padding-right: 10px;
  }
`;

const TimeIcon = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  padding-right: 20px;
`;
const UserAvatar = styled(AccountBoxIcon)``;

const Search = styled.div`
  display: flex;
  flex: 2;
  background-color: #421f44;
  border-radius: 6px;
  padding: 0 50px;
  text-align: center;
  border: 1px gray solid;
  > input {
    background-color: transparent;
    border: none;
    min-width: 30vw;
    outline: none;
    color: #fff;
  }
`;
