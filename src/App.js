import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import styled from "styled-components";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar/Sidebar";

function App() {
  return (
    <div>
      <Header />
      <Router>
        <AppBody>
          <Sidebar />
        </AppBody>
        {/* <Routes>
          <Route path="/" element={<Header />}></Route>
        </Routes> */}
      </Router>
    </div>
  );
}

export default App;

const AppBody = styled.div`
  display: flex;
`;
